

module.exports.optionDefinitions = [{
    name: 'checkinterval',
    alias: 'i',
    type: Number,
    defaultValue: 200
},
{
    name: 'remaptime',
    alias: 'r',
    type: Number,
    defaultValue: 5000
}
]