const SerialPort = require('serialport')
const Readline = require('@serialport/parser-readline')
const WebSocket = require('ws')
const commandLineArgs = require('command-line-args')
const utils = require('./utils.js')
var express = require('express');
var app = express();

app.use(express.static('frontend'));

app.listen(3000, function () {
  console.log('Example app listening on port 3000!');
});


let serialdevices = []
let productids = []
let parsers = []
let sockets = []

let messages = []
const messagecount = 100

let serialStatus

const options = commandLineArgs(utils.optionDefinitions)

console.log(options)


const wss = new WebSocket.Server({ port: 8080 });



function checkSerial() {
    SerialPort.list()
        .then((data) => {
            data.forEach((elem) => {
                if (elem.locationId) {
                    let found = 0
                    serialdevices.forEach((arduino) => {
                        if (arduino.path == elem.comName) {
                            found++
                        }
                    })
                    if (!found) {
                        let idCounter = 0
                        productids.forEach((dev) => {
                            if (elem.productId == dev) {
                                idCounter++
                            }
                        })
                        if (!idCounter) {
                            createSerialPort(elem.comName, elem.productId)
                        }
                    }
                }
            })
            if (serialdevices.length != serialStatus) {
                serialStatus = serialdevices.length
                broadcastFrontend()
                console.log(serialStatus + " devices connected!")
            }
        })
        .catch((err) => {
            console.log(err)
        })
}

function createSerialPort(name, id) {
    const port = new SerialPort(name, {baudRate: 115200})

    port.on('error', (err => {
        console.log(err.message)
    }))

    port.on('open', () => {
        console.log('opening port on ' + port.path)
    })

    port.on('close', () => {
        console.log("DISCONNECT!")
        serialdevices.splice(serialdevices.indexOf(port), 1)
        productids.splice(productids.indexOf(id), 1)
        parsers.splice(parsers.indexOf(parser), 1)
    })
    port.on('error', (err) => {
        console.log(err.message)
    })

    const parser = port.pipe(new Readline({ delimiter: '\r\n' }))
    parser.route = []
    //console.log(parser.route)

    parser.on('data', (data) => {
        if (parser.route) {
            parser.route.forEach((elem) => {
                try {
                    sockets[elem].send(data)
                }
                catch (err) {
                    console.log(err)
                }
            })
        }
        console.log(data)
        addToMessages({id: name, msg: data})
        broadcastFrontend()
    })
    serialdevices.push(port)
    productids.push(id)
    parsers.push(parser)
}

wss.on('connection', (ws, req) => {
    console.log('new connection! ')

    ws.on('message', (message) => {
        console.log('received: %s', message)
    })

    ws.on('close', () => {
        console.log("socket removed!")

        sockets.splice(sockets.indexOf(ws), 1)
        console.log("new socket count: " + sockets.length)
        broadcastFrontend()
    })

    ws.on('message', (msg) => {
        ws.send('nope')
    })

    sockets.push(ws)
    broadcastFrontend()
    registerNewOutput(ws)
})


const frontend = new WebSocket.Server({ port: 8081 });

frontend.on('connection', () => {
    broadcastFrontend()
})

function broadcastFrontend(){
    let current = currentStats()
    console.log(current)
    frontend.clients.forEach((client) => {
        client.send(JSON.stringify(current));
        console.log("update")
    })
}

function addToMessages(inMessage){
    if(messages.length == messagecount){
        messages.shift()
    }
    messages.push(inMessage)
}


frontend.on('connection', (ws, req) => {
    console.log('frontend connected!')
    broadcastFrontend()

    ws.on('message', (message) => {
        console.log('received: %s', message)
    })

    ws.on('close', () => {
        console.log("frontend disconnected!")
    })
})

function currentStats(){
    return {
        serial: parsers.length,
        sockets: sockets.length,
        messages: messages
    }
}

function remapConnections() {
    parsers.forEach((elem) => {
        elem.route = []
    })
    //console.log("remapping connections!")
    let availableInputs = getInputArray(parsers.length)

    sockets.forEach((elem) => {
        if (availableInputs.length == 0) {
            availableInputs = getInputArray(parsers.length)
        }
        const index = Math.floor(Math.random() * availableInputs.length)
        const inputIndex = availableInputs[index]
        parsers[inputIndex].route.push(sockets.indexOf(elem))
        availableInputs.splice(index, 1)
    })
}

function getInputArray(length) {
    let inputs = []
    for (let i = 0; i < length; i++) {
        inputs.push(i)
    }
    return inputs
}

function registerNewOutput(output) {
    let inputIndex = 0
    let connections = 0
    parsers.forEach((elem) => {
        if (elem.route.length > connections) {
            connections = elem.route.length
            inputIndex = parsers.indexOf(elem)
        }
    })

    const outputIndex = sockets.indexOf(output)
    console.log("registering new output: " + inputIndex + " -> " + outputIndex)
    if(parsers.length > 0){
        parsers[inputIndex].route.push(outputIndex)
    }
}

let scanner = setInterval(checkSerial, options.checkinterval)
let shuffler = (() => {
    if (options.remaptime != 0) {
        return setInterval(remapConnections, options.remaptime)
    }
})
