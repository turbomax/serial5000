console.log("moin");

var connection = new WebSocket('ws://' + location.hostname + ':8081');

let oldData = {
    serial: 0,
    sockets: 0
};


connection.onopen = function () {
    console.log("opened ws!")
};

// Log errors
connection.onerror = function (error) {
    console.log('WebSocket Error ' + error);
};

// Log messages from the server
connection.onmessage = function (e) {
    //console.log('Server: ' + e.data);
    const inData = JSON.parse(e.data);

    if (oldData.serial != inData.serial) {
        let serialDisplay = document.getElementById("serial");
        serialDisplay.innerHTML = inData.serial;
        serialDisplay.style.color = "red";

        setTimeout(() => {
            document.getElementById("serial").style.color = "white";
        }, 1000)
    }

    if (oldData.sockets != inData.sockets) {
        let socketDisplay = document.getElementById("sockets");
        socketDisplay.innerHTML = inData.sockets;
        socketDisplay.style.color = "red";

        setTimeout(() => {
            document.getElementById("sockets").style.color = "white";
        }, 1000)
    }

    if(inData.messages.length > 0){
        let content = ""
        inData.messages.reverse().forEach(element => {
            if(element != undefined){
                content += (element.id + ': ' + element.msg + '<br>')
            }
        })
        document.getElementById('console').innerHTML = content

    }
    oldData = inData;

};

function isNew(newData) {
    return ((newData.serial != oldData.serial) + (newData.sockets != oldData.sockets)) > 0
}

